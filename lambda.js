const awsServerlessExpress = require('aws-serverless-express');
const aws = require('aws-sdk');
const credentials = require('./.aws/credentials.json');
aws.config.update({
    'credentials': {
        'accessKeyId': credentials.aws_access_key_id,
        'secretAccessKey': credentials.aws_secret_access_key
    },
    'region': credentials.region
});
const app = require('./dist/app');

const server = awsServerlessExpress.createServer(app)

module.exports.universal = (event, context) => awsServerlessExpress.proxy(server, event, context);
