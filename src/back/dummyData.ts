import {User} from "./models/User";
import {Group} from "./models/Group";
import {Investment} from "./models/Investment";
import {Loan, LoanStatus} from "./models/Loan";
export function dummydata() {

    let groupId: String;
    let promises: Promise<any>[] = [];
    for (let i = 1; i < 10; i++) {
        promises.push(new Promise(((resolve, reject) => {
            let group = new Group(`Group ${i}`, 'fi', true, 'default');
            group.create((err: any, data: any) => {
                if (err) reject(err);
                resolve(data);
            })
        })))
    }

    Promise.all(promises)
        .then((groups: Group[]) => {
            groupId = groups[2].id;
            let promises: Promise<any>[] = [];
            for (let i = 1; i < 10; i++) {
                promises.push(new Promise((resolve, reject) => {
                    let username = `test${i}@test.com`;
                    let user = new User(username, `Tester ${i}`, `test${i}@test.com`, 'Test Street 23', 'City Test', 'fi');
                    user.groups.push(groupId);
                    user.create('abcd', (err: any, data: any) => {
                        if (err) reject(err);
                        resolve(data);
                    });
                }));
            }
            return Promise.all(promises);
        })
        .then(() => {
            let promises: Promise<any>[] = [];
            for (let i = 5; i < 10; i++) {
                promises.push(new Promise((resolve, reject) => {
                    let investment = new Investment(`test${i}@test.com`, Math.floor(Math.random() * 1000 + 500), groupId);
                    investment.create((err: any, data: any) => {
                        if (err) reject(err);
                        resolve(data);
                    })
                }));
            }
            return Promise.all(promises);
        })
        .then(() => {
            let promises: Promise<any>[] = [];
            for (let i = 2; i < 8; i++) {
                promises.push(new Promise((resolve, reject) => {
                    let date = new Date();
                    date.setMonth(i);
                    let loan = new Loan(`test4@test.com`, Math.floor(Math.random() * 500), groupId, date, 0, Math.floor(Math.random() * 5), [], LoanStatus.NEW);
                    loan.create((err: any, data: any) => {
                        if (err) reject(err);
                        resolve(data);
                    });
                }));
            }
            return Promise.all(promises);
        });
}