import { DynamoDB, config } from 'aws-sdk'

export class DB {
    private static instance: DB;
    private db: DynamoDB;
    private docClient: DynamoDB.DocumentClient;
    static getInstance() {
        if (DB.instance == null) {
            DB.instance = new DB();
        }
        return DB.instance;
    }
    constructor() {
        this.db = new DynamoDB();
        this.docClient = new DynamoDB.DocumentClient();
    }
    getDb() {
        return this.db;
    }
    getClient() {
        return this.docClient;
    }
    createTables() {
        let databaseStructure: DynamoDB.Types.CreateTableInput[] = [
            {
                TableName: 'User',
                KeySchema: [
                    { AttributeName: 'id', KeyType: 'HASH' },
                    { AttributeName: 'username', KeyType: 'RANGE' },
                ],
                AttributeDefinitions: [
                    { AttributeName: 'id', AttributeType: 'S' },
                    { AttributeName: 'username', AttributeType: 'S' },
                ],
                ProvisionedThroughput: {
                    ReadCapacityUnits: 10,
                    WriteCapacityUnits: 10
                }
            },
            {
                TableName: 'Group',
                KeySchema: [
                    { AttributeName: 'id', KeyType: 'HASH' }
                ],
                AttributeDefinitions: [
                    { AttributeName: 'id', AttributeType: 'S' }
                ],
                ProvisionedThroughput: {
                    ReadCapacityUnits: 10,
                    WriteCapacityUnits: 10
                }
            },
            {
                TableName: 'Loan',
                KeySchema: [
                    { AttributeName: 'id', KeyType: 'HASH' },
                ],
                AttributeDefinitions: [
                    { AttributeName: 'id', AttributeType: 'S' }
                ],
                ProvisionedThroughput: {
                    ReadCapacityUnits: 10,
                    WriteCapacityUnits: 10
                }
            },
            {
                TableName: 'Investment',
                KeySchema: [
                    { AttributeName: 'id', KeyType: 'HASH' }
                ],
                AttributeDefinitions: [
                    { AttributeName: 'id', AttributeType: 'S' }
                ],
                ProvisionedThroughput: {
                    ReadCapacityUnits: 10,
                    WriteCapacityUnits: 10
                }
            },
            {
                TableName: 'Request',
                KeySchema: [
                    { AttributeName: 'id', KeyType: 'HASH' }
                ],
                AttributeDefinitions: [
                    { AttributeName: 'id', AttributeType: 'S' }
                ],
                ProvisionedThroughput: {
                    ReadCapacityUnits: 10,
                    WriteCapacityUnits: 10
                }
            }
        ];

        databaseStructure.forEach( ( structure ) => {
            console.log( `Create table ${structure.TableName}` );
            this.db.deleteTable({ TableName: structure.TableName }, (err, data) => {
                if ( err ) {
                    console.error( 'Unable to delete table. Error JSON:', JSON.stringify( err, null, 2 ) );
                } else {
                    console.log( 'Deleted table. Table description JSON:', JSON.stringify( data, null, 2 ) );
                }
                this.db.createTable( structure, ( err, data ) => {
                    if ( err ) {
                        console.error( 'Unable to create table. Error JSON:', JSON.stringify( err, null, 2 ) );
                    } else {
                        console.log( 'Created table. Table description JSON:', JSON.stringify( data, null, 2 ) );
                    }
                } );
            });
        } );
    }
}
