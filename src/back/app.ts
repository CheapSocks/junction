import * as express from 'express'
import * as bodyParser from 'body-parser'
import { DB } from './db'
import {UserRoutes} from './routes/User';
import {GroupRoutes} from './routes/Group';
import {LoanRoutes} from './routes/Loan';
import {InvestmentRoutes} from './routes/Investment';
import {RequestRoute} from './routes/Request';
import {Loan, LoanStatus} from './models/Loan';
import {dummydata} from './dummyData';
const db = DB.getInstance();

//dummydata();

Loan.getLoansWithStatus(LoanStatus.NEW, (err: any, loans: Loan[]) => {
    loans.forEach((loan: Loan) => {
        loan.processNewLoan();
    });
});

// db.createTables();

const app = express();

app.use( bodyParser.json() );
app.use( express.static( 'public' ) );
app.use( '/production', express.static( 'public' ) );

/**
 * Query would be
 *      /db/User?id=4
 */
app.get( '/db/:tableName', ( req, res ) => {
    let params = {
        TableName: req.params.tableName,
        Key: {
            id: req.query.id
        }
    };
    db.getClient().get(params, function (err, data) {
        if (err) {
            console.error('Unable to add item. Error JSON:', JSON.stringify(err, null, 2));
        } else {
            res.status(200).send(JSON.stringify(data, null, 2));
        }
    });
} );

// Add User routes
new UserRoutes(app);

// Add Group routes
new GroupRoutes(app);

// Add Loan routes
new LoanRoutes(app);

// Add Investment routes
new InvestmentRoutes(app);

// Add Request routes
new RequestRoute(app);

// Server
module.exports = app;
