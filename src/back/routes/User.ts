
import {User} from '../models/User';
import {Express} from 'express';

export class UserRoutes {
    constructor(app: Express) {

        app.get('/user', (req, res) => {
            User.getUsers((err: any, data: User[]) => {
                if (err) {
                    res.status(500).send({error: err});
                } else {
                    res.status(200).send(data);
                }
            });
        });

        app.post('/user', (req, res) => {
            let user = new User(req.body.username, req.body.name, req.body.mail, req.body.address, req.body.city,
                req.body.country);
            user.create(req.body.password, (err: any, data: User) => {
                if (err) {
                    res.status(500).send({error: err});
                } else {
                    res.status(200).send({id: data.id});
                }
            });
        });

        app.get('/user/:username', (req, res) => {
            User.getUser(req.params.username, (err: any, data: User) => {
                if (err) {
                    console.error(err);
                    res.status(404).send({error: err});
                } else {
                    data.fetchGroups()
                        .then((user: User) => {
                            return user.fetchLoans();
                        })
                        .then((user: User) => {
                            return user.fetchInvestments();
                        })
                        .then((user: User) => {
                            return user.fetchRequests();
                        })
                        .then((user: User) => {
                            res.status(200).send(user);
                        })
                        .catch((err) => {
                            res.status(500).send(err);
                        });
                }
            });
        });

        app.post('/user/:username', (req, res) => {
            User.getUser(req.params.username, (err: any, user: any) => {
                if (err) {
                    console.error(err);
                    res.status(404).send({error: err});
                } else {
                    Object.keys(req.body).forEach((key) => {
                        user[key] = req.body[key];
                    });
                    user.update((err: any, user: User) => {
                        if (err) {
                            res.status(500).send(err);
                        } else {
                            res.status(200).send(user);
                        }
                    });
                }
            });
        });

        app.put('/user/:username', (req, res) => {
            User.getUser(req.params.username, (err: any, user: any) => {
                if (err) {
                    console.error(err);
                    res.status(404).send({error: err});
                } else {
                    console.log(req.body);
                    Object.keys(req.body).forEach((key) => {
                        user[key] = req.body[key];
                    });
                    user.update((err: any, user: User) => {
                        if (err) {
                            res.status(500).send(err);
                        } else {
                            res.status(200).send(user);
                        }
                    });

                }
            });
        });

        app.post('/user/:username/login', (req, res) => {
            User.auth(req.body.username, req.body.password, (err: any, data: User) => {
                if (err) {
                    console.error(err);
                    res.status(500).send({error: err});
                } else {
                    data.fetchGroups()
                        .then((user: User) => {
                            return user.fetchLoans();
                        })
                        .then((user: User) => {
                            return user.fetchInvestments();
                        })
                        .then((user: User) => {
                            return user.fetchRequests();
                        })
                        .then((user: User) => {
                            res.status(200).send(user);
                        })
                        .catch((err) => {
                            res.status(500).send(err);
                        });
                }
            })
        });
    }
}