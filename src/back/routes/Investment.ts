import {Express} from 'express';
import {Investment} from '../models/Investment';

export class InvestmentRoutes {

    constructor(app: Express) {
        app.get('/investment', (req, res) => {
            Investment.getInvestments((err: any, data: Investment[]) => {
                if (err) {
                    res.status(500).send({error: err});
                } else {
                    res.status(200).send(data);
                }
            });
        });

        app.post('/investment', (req, res) => {
            let investment = new Investment(req.body.username, req.body.amount, req.body.group);
            investment.create((err: any, data: Investment) => {
                if (err) {
                    res.status(500).send({error: err});
                } else {
                    res.status(200).send({id: data.id});
                }
            });
        });

        app.post('/group/:groupid/invest', (req, res) => {
            let investment = new Investment(req.body.username, req.body.amount, req.params.groupid);
            investment.create((err: any, data: Investment) => {
                if (err) {
                    res.status(500).send({error: err});
                } else {
                    res.status(200).send({id: data.id});
                }
            });
        });

        app.get('/investment/:investmentid', (req, res) => {
            Investment.getInvestment(req.params.investmentid, (err: any, data: Investment) => {
                if (err) {
                    console.error(err);
                    res.status(404).send({error: err});
                } else {
                    res.status(200).send(data);
                }
            });
        });

    }

}