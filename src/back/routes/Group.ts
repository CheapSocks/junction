import {Group} from '../models/Group';
import {Express} from 'express';

export class GroupRoutes {

    constructor(app: Express) {
        app.get('/group', (req, res) => {
            Group.getGroups((err: any, data: Group[]) => {
                if (err) {
                    res.status(500).send({error: err});
                } else {
                    res.status(200).send(data);
                }
            });
        });

        app.post('/group', (req, res) => {
            let group = new Group(req.body.name, req.body.country, req.body.isPublic, req.body.trustService);
            group.create((err: any, data: Group) => {
                if (err) {
                    res.status(500).send({error: err});
                } else {
                    res.status(200).send({id: data.id});
                }
            });
        });

        app.get('/group/:groupid', (req, res) => {
            Group.getGroup(req.params.groupid, (err: any, data: Group) => {
                if (err) {
                    console.error(err);
                    res.status(404).send({error: err});
                } else {
                    res.status(200).send(data);
                }
            });
        });

        app.post('/group/:groupid', (req, res) => {
            Group.getGroup(req.params.groupid, (err: any, group: any) => {
                if (err) {
                    console.error(err);
                    res.status(404).send({error: err});
                } else {
                    Object.keys(req.body).forEach((key) => {
                        group[key] = req.body[key];
                    });
                    group.update();
                    res.status(200).send(group);
                }
            });
        });

        app.put('/group/:groupid', (req, res) => {
            Group.getGroup(req.params.groupid, (err: any, group: any) => {
                if (err) {
                    console.error(err);
                    res.status(404).send({error: err});
                } else {
                    Object.keys(req.body).forEach((key) => {
                        group[key] = req.body[key];
                    });
                    group.update();
                    res.status(200).send(group);
                }
            });
        });

    }

}