import {Express} from 'express';
import {Request, RequestStatus} from '../models/Request';

export class RequestRoute {

    constructor(app: Express) {
        app.get('/request', (req, res) => {
            Request.getRequests((err: any, data: Request[]) => {
                if (err) {
                    res.status(500).send({error: err});
                } else {
                    res.status(200).send(data);
                }
            });
        });

        app.post('/request', (req, res) => {
            let request = new Request(req.body.username, req.body.amount, req.body.group, RequestStatus.NEW, new Date());
            request.create((err: any, data: Request) => {
                if (err) {
                    res.status(500).send({error: err});
                } else {
                    res.status(200).send({id: data.id});
                }
            });
        });

        app.post('/group/:groupid/request', (req, res) => {
            let request = new Request(req.body.username, req.body.amount, req.params.groupid, RequestStatus.NEW, new Date());
            request.create((err: any, data: Request) => {
                if (err) {
                    res.status(500).send({error: err});
                } else {
                    res.status(200).send({id: data.id});
                }
            });
        });

        app.get('/request/:requestid', (req, res) => {
            Request.getRequest(req.params.requestid, (err: any, data: Request) => {
                if (err) {
                    console.error(err);
                    res.status(404).send({error: err});
                } else {
                    res.status(200).send(data);
                }
            });
        });

    }

}