import {Express} from 'express';
import {Loan, LoanStatus} from '../models/Loan';

export class LoanRoutes {

    constructor(app: Express) {
        app.get('/loan', (req, res) => {
            Loan.getLoans((err: any, data: Loan[]) => {
                if (err) {
                    res.status(500).send({error: err});
                } else {
                    res.status(200).send(data);
                }
            });
        });

        app.post('/loan', (req, res) => {
            let loan = new Loan(req.body.username, req.body.amount, req.body.group, new Date(), req.params.interest, req.body.length, [], LoanStatus.NEW);
            loan.create((err: any, data: Loan) => {
                if (err) {
                    res.status(500).send({error: err});
                } else {
                    res.status(200).send({id: data.id});
                }
            });
        });

        app.post('/group/:groupid/loan', (req, res) => {
            let loan = new Loan(req.body.username, req.body.amount, req.params.groupid, new Date(), req.params.interest, req.body.length, [], LoanStatus.NEW);
            loan.create((err: any, data: Loan) => {
                if (err) {
                    res.status(500).send({error: err});
                } else {
                    res.status(200).send({id: data.id});
                }
            });
        });

        app.get('/loan/:loanid', (req, res) => {
            Loan.getLoan(req.params.loanid, (err: any, data: Loan) => {
                if (err) {
                    console.error(err);
                    res.status(404).send({error: err});
                } else {
                    res.status(200).send(data);
                }
            });
        });

    }

}