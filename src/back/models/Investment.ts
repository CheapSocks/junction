import * as hash from 'object-hash'
import { DB } from '../db';
import {User} from './User';
import {LoanStatus} from "./Loan";
const db = DB.getInstance();
const tableName = 'Investment';

/**
 * Investment entity:
 *   * User
 *   * Amount
 *   * Group
 *
 */
export class Investment {

    id: String;
    username: String;
    amount: number;
    group: String;

    // calculate values
    availableFunds = 0;
    pendingFunds = 0;

    static getInvestment(investmentId: String, callback: Function) {
        const params = {
            TableName: tableName,
            Key: {
                id: investmentId
            }
        };
        db.getClient().get(params, (err, data) => {
            if (err) {
                callback(err, data);
            } else if (data.Item) {
                callback(err, Investment.createInvestment(data.Item));
            } else {
                callback({name: 'Investment not found'}, null);
            }
        });
    }

    static getInvestments(callback: Function) {
        let params = {
            TableName : tableName,
        };

        db.getClient().scan(params, function(err, data) {
            if (err) {
                console.error('Unable to query. Error:', JSON.stringify(err, null, 2));
                callback(err, []);
            } else {
                callback(err, Investment.processData(data.Items));
            }
        });
    }

    static getInvestmentsFromUser( userName: String, callback: Function ) {
        let params = {
            TableName: tableName,
            FilterExpression: '#user = :username',
            ExpressionAttributeNames: {
                '#user': 'username',
            },
            ExpressionAttributeValues: {
                ':username': userName,
            }
        };

        db.getClient().scan( params, function ( err, data ) {
            if ( err ) {
                console.error( 'Unable to query. Error:', JSON.stringify( err, null, 2 ) );
                callback( err, [] );
            } else {
                callback( err, Investment.processData(data.Items) );
            }
        } );
    }

    static getInvestmentsFromGroup(groupid: String, callback: Function) {
        let params = {
            TableName: tableName,
            FilterExpression: '#group = :group',
            ExpressionAttributeNames: {
                '#group': 'group',
            },
            ExpressionAttributeValues: {
                ':group': groupid,
            }
        };

        db.getClient().scan( params, function ( err, data ) {
            if ( err ) {
                console.error( 'Unable to query. Error:', JSON.stringify( err, null, 2 ) );
                callback( err, [] );
            } else {
                callback( err, Investment.processData(data.Items) );
            }
        } );
    }

    private static processData(items: any) {
        let investments: Investment[] = [];
        items.forEach((data: any) => {
            investments.push(Investment.createInvestment(data));
        });
        return investments;
    }

    private static createInvestment(data: any) {
        return new Investment(data.username, parseFloat(data.amount), data.group, data.id);
    }

    constructor(username: String, amount: number, group: String, id?: String) {
        this.username = username;
        this.amount = parseFloat(amount.toString());
        this.group = group;
        if (id) {
            this.id = id;
        }
    }

    create(callback: Function) {
        const params = {
            TableName: tableName,
            Item: {
                'id': hash({name: this.username, group: this.group}),
                'username': this.username,
                'amount': parseFloat(this.amount.toString()),
                'group': this.group,
            }
        };
        User.getUser(this.username, (err: any, user: User) => {
            if (err) {
                callback({error: `User ${this.username} not found`}, null);
                return;
            }
            user.fetchLoans()
                .then((user: User) => {
                    return user.fetchInvestments();
                }).then((user: User) => {
                    let openLoan = false;
                    user.loansData.forEach((loan) => {
                        if (loan.status !== LoanStatus.PAID && loan.group === this.group) {
                            openLoan = true;
                        }
                    });
                    if (openLoan) {
                        callback({error: 'There is at least one open loan. You can\'t invest before all loans are paid.'}, null);
                        return;
                    }
                    let investmentExists = false;
                    user.investmentsData.forEach((investment) => {
                        if (investment.group === this.group) {
                            investmentExists = true;
                        }
                    });
                    if (investmentExists) {
                        callback({error: 'There is already an investment, please create a request to change the investment for this group.'}, null);
                        return;
                    }
                    db.getClient().put(params, (err, data) => {
                        if (err) {
                            console.error('Unable to add item. Error JSON:', JSON.stringify(err, null, 2));
                            callback(err, data);
                        } else {
                            console.log('Added item:', JSON.stringify(data, null, 2));
                            this.id = params.Item.id;
                            user.updateBalance(this.amount, () => {
                                callback(err, this);
                            });
                        }
                    });
                });
        });
    }

    updateAmount(change: number, callback: Function) {
        const params = {
            TableName: tableName,
            Key: {
                'id': this.id
            },
            UpdateExpression: 'set amount = amount + :change',
            ExpressionAttributeValues: {
                ':change': change,
            },
            ReturnValues: 'UPDATED_NEW'
        };

        User.getUser(this.username, (err: any, user: User) => {
            if (err) {
                callback({error: `User ${this.username} not found`}, null);
                return;
            }
            db.getClient().update(params, (err, data) => {
                if (err) {
                    console.error('Unable to update item. Error JSON:', JSON.stringify(err, null, 2));
                    callback(err, data);
                } else {
                    console.log('UpdateItem succeeded:', JSON.stringify(data, null, 2));
                    user.updateBalance(change, () => {
                        callback(null, this);
                    });
                }
            });
        });
    }
}