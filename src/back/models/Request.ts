import * as hash from 'object-hash'
import { DB } from '../db';
import {User} from './User';
import {Investment} from './Investment';
const db = DB.getInstance();
const tableName = 'Request';

export enum RequestStatus {
    NEW = 'New',
    WAITING = 'Waiting',
    DONE = 'Done',
}


/**
 * Request entity:
 *   * User
 *   * Amount
 *   * Group
 *   * Status
 *   * Created
 */
export class Request {

    id: String;
    username: String;
    amount: number;
    group: String;
    status: String;
    created: Date;

    static getRequest(requestId: String, callback: Function) {
        const params = {
            TableName: tableName,
            Key: {
                id: requestId
            }
        };
        db.getClient().get(params, (err, data) => {
            if (err) {
                callback(err, data);
            } else if (data.Item) {
                callback(err, Request.createRequest(data.Item));
            } else {
                callback({name: 'Request not found'}, null);
            }
        });
    }

    static getRequests(callback: Function) {
        let params = {
            TableName : tableName,
        };

        db.getClient().scan(params, function(err, data) {
            if (err) {
                console.error('Unable to query. Error:', JSON.stringify(err, null, 2));
                callback(err, []);
            } else {
                callback(err, Request.processData(data.Items));
            }
        });
    }


    static getRequestsFromUser( userName: String, callback: Function ) {
        let params = {
            TableName: tableName,
            FilterExpression: '#user = :username',
            ExpressionAttributeNames: {
                '#user': 'username',
            },
            ExpressionAttributeValues: {
                ':username': userName,
            }
        };

        db.getClient().scan( params, function ( err, data ) {
            if ( err ) {
                console.error( 'Unable to query. Error:', JSON.stringify( err, null, 2 ) );
                callback( err, [] );
            } else {
                callback( err, Request.processData(data.Items) );
            }
        } );
    }

    static getRequestsFromGroup(groupid: String, callback: Function) {
        let params = {
            TableName: tableName,
            FilterExpression: '#group = :group',
            ExpressionAttributeNames: {
                '#group': 'group',
            },
            ExpressionAttributeValues: {
                ':group': groupid,
            }
        };

        db.getClient().scan( params, function ( err, data ) {
            if ( err ) {
                console.error( 'Unable to query. Error:', JSON.stringify( err, null, 2 ) );
                callback( err, [] );
            } else {
                callback( err, Request.processData(data.Items) );
            }
        } );
    }

    private static processData(items: any) {
        let requests: Request[] = [];
        items.forEach((data: any) => {
            requests.push(Request.createRequest(data));
        });
        return requests;
    }

    private static createRequest(data: any) {
        return new Request(data.username, parseFloat(data.amount), data.group, data.status, new Date(data.created), data.id);
    }

    constructor(username: String, amount: number, group: String, status: String, created: Date, id?: String) {
        this.username = username;
        this.amount = parseFloat(amount.toString());
        this.group = group;
        this.status = status;
        this.created = created;
        if (id) {
            this.id = id;
        }
    }

    create(callback: Function) {
        const params = {
            TableName: tableName,
            Item: {
                'id': hash({name: this.username, group: this.group, created: this.created}),
                'username': this.username,
                'amount': parseFloat(this.amount.toString()),
                'group': this.group,
                'status': this.status,
                'created': this.created.toISOString(),
            }
        };
        User.getUser(this.username, (err: any, user: User) => {
            if (err) {
                callback(err, null);
                return;
            }
            if (this.amount > user.balance) {
                callback({error: `Not allowed to withdraw more than ${user.balance}`}, null);
                return;
            }
            db.getClient().put(params, (err, data) => {
                if (err) {
                    console.error('Unable to add item. Error JSON:', JSON.stringify(err, null, 2));
                    callback(err, data);
                } else {
                    console.log('Added item:', JSON.stringify(data, null, 2));
                    this.id = params.Item.id;
                    this.processRequest();
                    callback(err, this);
                }
            });
        });
    }

    update() {
        const params = {
            TableName: tableName,
            Key: {
                'id': this.id
            },
            UpdateExpression: 'set #status = :status',
            ExpressionAttributeNames: {
                '#status': 'status'
            },
            ExpressionAttributeValues: {
                ':status': this.status
            },
            ReturnValues: 'UPDATED_NEW'
        };

        db.getClient().update(params, function(err, data) {
            if (err) {
                console.error('Unable to update item. Error JSON:', JSON.stringify(err, null, 2));
            } else {
                console.log('UpdateItem succeeded:', JSON.stringify(data, null, 2));
            }
        });
    }

    processRequest() {
        Investment.getInvestmentsFromUser(this.username, (err: any, investments: Investment[]) => {
            investments.forEach((investment: Investment) => {
                if (investment.group === this.group) {
                    investment.updateAmount(this.amount * -1, () => {
                        User.getUser(this.username, (err: any, user: User) => {
                            user.updateBalance(this.amount * -1);
                            this.status = RequestStatus.DONE;
                            this.update();
                        });
                    })
                }
            });
        });
    }

}