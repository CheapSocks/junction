import * as hash from 'object-hash'
import { DB } from '../db';
import {User} from './User';
import {Group} from './Group';
const db = DB.getInstance();
const tableName = 'Loan';

export enum LoanStatus {
    NEW = 'New',
    RUNNING = 'Running',
    UNPAID = 'Unpaid',
    PAID = 'Paid',
}

interface Debtee {
    username: String;
    amount: number;
}

/**
 * Loan entity:
 *   * User
 *   * Amount
 *   * Group
 *   * Created date
 *   * Start of money get in
 *   * Interest
 *   * Length
 *   * Debtee
 *
 */
export class Loan {

    id: String;
    username: String;
    amount: number;
    group: String;
    created: Date;
    interest: number;
    length: number;
    status: String;
    debtees: Debtee[];

    static getLoan( loanId: String, callback: Function ) {
        const params = {
            TableName: tableName,
            Key: {
                id: loanId
            }
        };
        db.getClient().get( params, ( err, data ) => {
            if ( err ) {
                callback( err, data );
            } else if ( data.Item ) {
                callback( err, Loan.createLoan(data.Item) );
            } else {
                callback( { name: 'Loan not found' }, null );
            }
        } );
    }

    static getLoans( callback: Function ) {
        let params = {
            TableName: tableName,
        };

        db.getClient().scan( params, function ( err, data ) {
            if ( err ) {
                console.error( 'Unable to query. Error:', JSON.stringify( err, null, 2 ) );
                callback( err, [] );
            } else {
                callback( err, data.Items );
            }
        } );
    }

    static getLoansFromUser( userName: String, callback: Function ) {
        let params = {
            TableName: tableName,
            FilterExpression: '#user = :username',
            ExpressionAttributeNames: {
                '#user': 'username',
            },
            ExpressionAttributeValues: {
                ':username': userName,
            }
        };

        db.getClient().scan( params, function ( err, data ) {
            if ( err ) {
                console.error( 'Unable to query. Error:', JSON.stringify( err, null, 2 ) );
                callback( err, [] );
            } else {
                callback( err, Loan.processData(data.Items) );
            }
        } );
    }

    static getLoansFromGroup(groupid: String, callback: Function) {
        let params = {
            TableName: tableName,
            FilterExpression: '#group = :group',
            ExpressionAttributeNames: {
                '#group': 'group',
            },
            ExpressionAttributeValues: {
                ':group': groupid,
            }
        };

        db.getClient().scan( params, function ( err, data ) {
            if ( err ) {
                console.error( 'Unable to query. Error:', JSON.stringify( err, null, 2 ) );
                callback( err, [] );
            } else {
                callback( err, Loan.processData(data.Items) );
            }
        } );
    }

    static getLoansWithStatus(status: String, callback: Function) {
        let params = {
            TableName: tableName,
            FilterExpression: '#status = :status',
            ExpressionAttributeNames: {
                '#status': 'status',
            },
            ExpressionAttributeValues: {
                ':status': status,
            }
        };

        db.getClient().scan( params, function ( err, data ) {
            if ( err ) {
                console.error( 'Unable to query. Error:', JSON.stringify( err, null, 2 ) );
                callback( err, [] );
            } else {
                callback( err, Loan.processData(data.Items) );
            }
        } );
    }

    private static processData(items: any) {
        let loans: Loan[] = [];
        items.forEach((data: any) => {
            loans.push(Loan.createLoan(data));
        });
        return loans;
    }

    private static createLoan(data: any) {
        return new Loan( data.username, parseFloat(data.amount), data.group, new Date(data.created), parseFloat(data.interest),
            parseFloat(data.length), data.debtees, data.status, data.id );
    }

    constructor(username: String, amount: number, group: String, created: Date, interest: number, length: number,
                debtees: Debtee[], status: String, id?: String ) {
        this.username = username;
        this.amount = parseFloat(amount.toString());
        this.group = group;
        this.created = created;
        this.interest = interest;
        this.length = length;
        this.debtees = debtees;
        this.status = status;
        if ( id ) {
            this.id = id;
        }
    }

    create( callback: Function ) {
        const params = {
            TableName: tableName,
            Item: {
                'id': hash( { name: this.username, country: this.created.toISOString() } ),
                'username': this.username,
                'amount': parseFloat(this.amount.toString()),
                'group': this.group,
                'created': this.created.toISOString(),
                'interest': this.interest,
                'length': this.length,
                'debtees': this.debtees,
                'status': this.status,
            }
        };

        User.getUser(this.username, (err: any, user: User) => {
            if (err) {
                callback(err, null);
                return;
            }
            user.fetchInvestments()
                .then((user: User) => {
                    let investmentExists = false;
                    user.investmentsData.forEach((investment) => {
                        if (investment.group === this.group && parseFloat(investment.amount.toString()) > 0) {
                            investmentExists = true;
                        }
                    });
                    if (investmentExists) {
                        callback({error: 'Investment for this group exists, first create a request to remove all investment.'});
                        return;
                    }
                    Group.getGroup(this.group, (err: any, group: Group) => {
                        if (err) {
                            callback(err, null);
                            return;
                        }
                        if (this.amount > group.availableFunds) {
                            callback({error: `Maximum possible loan ${group.availableFunds} but requested ${this.amount}`});
                            return;
                        }
                        db.getClient().put( params, ( err, data ) => {
                            if ( err ) {
                                console.error( 'Unable to add item. Error JSON:', JSON.stringify( err, null, 2 ) );
                                callback( err, data );
                            } else {
                                console.log( 'Added item:', JSON.stringify( data, null, 2 ) );
                                this.id = params.Item.id;
                                user.updateBalance(this.amount * -1, () => {
                                    // @todo: add to MQ
                                    this.processNewLoan();
                                    callback( err, this );
                                });
                            }
                        } )
                    });
                })
        });
    }

    update() {
        const params = {
            TableName: tableName,
            Key: {
                'id': this.id
            },
            UpdateExpression: 'set debtees = :debtees, interest = :interest, #status = :status',
            ExpressionAttributeNames: {
                '#status': 'status'
            },
            ExpressionAttributeValues: {
                ':debtees': this.debtees,
                ':interest': this.interest,
                ':status': this.status,
            },
            ReturnValues: 'UPDATED_NEW'
        };

        db.getClient().update( params, function ( err, data ) {
            if ( err ) {
                console.error( 'Unable to update item. Error JSON:', JSON.stringify( err, null, 2 ) );
            } else {
                console.log( 'UpdateItem succeeded:', JSON.stringify( data, null, 2 ) );
            }
        } );
    }

    processNewLoan() {
        Group.getGroup(this.group, (err: any, group: Group) => {
            const percentagePerUser = this.amount / group.availableFunds;
            User.getUsersByGroup(group.id, (err: any, users: User[]) => {
                const promises: Promise<any>[] = [];
                users.forEach((user: User) => {
                    promises.push(user.fetchInvestments());
                });
                Promise.all(promises).then((users: User[]) => {
                    users.forEach((user: User) => {
                        user.investmentsData.forEach((investment) => {
                            if (investment.group === group.id && investment.amount > 0) {
                                let lendAmount = investment.amount * percentagePerUser;
                                this.debtees.push({username: user.username, amount: lendAmount});
                                user.updateBalance(lendAmount * -1);
                            }
                        });
                    });
                    this.interest = Math.floor(Math.random() * (20 - 10 + 1) + 10); // random number between 10-20
                    this.interest = this.interest / 100;
                    this.status = LoanStatus.RUNNING;
                    this.update();
                });
            });
        });
    }
}