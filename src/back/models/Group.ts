import * as hash from 'object-hash'
import {User} from './User';
import { DB } from '../db';
import {Loan, LoanStatus} from './Loan';
import {Investment} from './Investment';
import {Request, RequestStatus} from './Request';
const db = DB.getInstance();
const tableName = 'Group';

/**
 * Group entity:
 *   * Name
 *   * Country
 *   * Members (provided over user)
 *   * Public/Private?
 *   * Trust Service?
 *
 */
export class Group {

    id: String;
    name: String;
    country: String;
    members: User[];
    isPublic: boolean;
    trustService: String;

    // calculated values
    loansData: Loan[];
    investmentsData: Investment[];
    requestsData: Request[];
    totalFunds = 0;
    availableFunds = 0;

    static getGroup(groupId: String, callback: Function) {
        const params = {
            TableName: tableName,
            Key: {
                id: groupId
            }
        };
        db.getClient().get(params, (err, data) => {
            if (err) {
                callback(err, data);
            } else if (data.Item) {
                let group = new Group(data.Item.name, data.Item.country, data.Item.isPublic, data.Item.trustService, data.Item.id);
                group.calculateFunds().then((group: Group) => {
                    callback(err, group);
                });
            } else {
                callback({name: 'Group not found'}, null);
            }
        });
    }

    static getGroups(callback: Function) {
        let params = {
            TableName : tableName,
        };

        db.getClient().scan(params, function(err, data) {
            if (err) {
                console.error('Unable to query. Error:', JSON.stringify(err, null, 2));
                callback(err, []);
            } else {
                callback(err, data.Items);
            }
        });
    }

    constructor(name: String, country: String, isPublic: boolean, trustService: String, id?: String) {
        this.name = name;
        this.country = country;
        this.isPublic = isPublic;
        this.trustService = trustService;
        if (id) {
            this.id = id;
        }
    }

    create(callback: Function) {
        const params = {
            TableName: tableName,
            Item: {
                'id': hash({name: this.name, country: this.country}),
                'name': this.name,
                'country': this.country,
                'isPublic': this.isPublic,
                'trustService': this.trustService,
            }
        };
        db.getClient().put(params, (err, data) => {
            if (err) {
                console.error('Unable to add item. Error JSON:', JSON.stringify(err, null, 2));
                callback(err, data);
            } else {
                console.log('Added item:', JSON.stringify(data, null, 2));
                this.id = params.Item.id;
                callback(err, this);
            }
        });
    }

    update() {
        const params = {
            TableName: tableName,
            Key: {
                'id': this.id
            },
            UpdateExpression: 'set #name = :name, country= :country, isPublic= :isPublic, trustService = :trustService',
            ExpressionAttributeNames: {
                '#name': 'name'
            },
            ExpressionAttributeValues: {
                ':name': this.name,
                ':country': this.country,
                ':isPublic': this.isPublic,
                ':trustService': this.trustService,
            },
            ReturnValues: 'UPDATED_NEW'
        };

        db.getClient().update(params, function(err, data) {
            if (err) {
                console.error('Unable to update item. Error JSON:', JSON.stringify(err, null, 2));
            } else {
                console.log('UpdateItem succeeded:', JSON.stringify(data, null, 2));
            }
        });
    }

    calculateFunds() {
        return this.fetchLoans()
            .then((group: Group) => {
                return group.fetchInvestments();
            })
            .then((group: Group) => {
                return group.fetchRequests();
            })
            .then((group: Group) => {
                this.investmentsData.forEach((investment) => {
                   this.totalFunds += parseFloat(investment.amount.toString());
                });
                this.availableFunds = this.totalFunds;
                this.loansData.forEach((loan) => {
                    if (loan.status === LoanStatus.PAID) {
                        return;
                    }
                   this.availableFunds -= parseFloat(loan.amount.toString());
                });
                this.requestsData.forEach((request) => {
                    if (request.status === RequestStatus.DONE) {
                        return;
                    }
                    this.availableFunds -= parseFloat(request.amount.toString());
                });
                if (this.availableFunds < 0) {
                    this.availableFunds = 0;
                }
                return this;
            })
    }

    fetchLoans() {
        return new Promise((resolve, reject) => {
            Loan.getLoansFromGroup(this.id, (err: any, loans: Loan[]) => {
                if (err) {
                    reject(err);
                }
                this.loansData = loans;
                resolve(this);
            });
        }).then(() => {
            return this;
        });
    }

    fetchInvestments() {
        return new Promise((resolve, reject) => {
            Investment.getInvestmentsFromGroup(this.id, (err: any, investments: Investment[]) => {
                if (err) {
                    reject(err);
                }
                this.investmentsData = investments;
                resolve(this);
            });
        }).then(() => {
            return this;
        });
    }

    fetchRequests() {
        return new Promise((resolve, reject) => {
            Request.getRequestsFromGroup(this.id, (err: any, requests: Request[]) => {
                if (err) {
                    reject(err);
                }
                this.requestsData = requests;
                resolve(this);
            });
        }).then(() => {
            return this;
        });
    }
}