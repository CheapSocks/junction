import { DB } from '../db';
import * as hash from 'object-hash'
import {createHmac} from 'crypto';
import {Group} from './Group';
import {Loan} from './Loan';
import {Investment} from './Investment';
import {Request} from './Request';
const db = DB.getInstance();

const tableName = 'User';

/**
 *  User entity
 *   * Name
 *   * Address
 *   * Mail
 *   * Bank information
 *   * Social Security Number
 *   * Trust for each group
 *   * Vouchers? guarantors
 *   * Groups
 *   * Balance
 */
export class User {
    id: String;
    username: String;
    name: String;
    mail: String;
    address: String;
    city: String;
    country: String;
    guarantors: String[];
    groups: String[];
    balance: Number;

    // for following data call fetchXXX before
    groupData: Group[] = [];
    loansData: Loan[] = [];
    investmentsData: Investment[] = [];
    requestsData: Request[] = [];

    static auth(username: String, password: string, callback: Function) {
        const params = {
            TableName: tableName,
            Key: {
                id: hash({username: username}),
                username: username
            }
        };
        db.getClient().get(params, (err, data) => {
            if (err) {
                callback(err, data);
            } else {
                const hashedPassword = User.createPasswordHash(password);
                if (data.Item && hashedPassword === data.Item.password) {
                    callback(null, User.createUser(data.Item));
                    return;
                }
                callback({msg: 'Wrong Password or Username'}, false);
            }
        });
    }

    static createPasswordHash(password: string) {
        let hash = createHmac('sha512', 'sdfgw4br6awe93nvasdav33'); /** Hashing algorithm sha512 */
        hash.update(password);
        return hash.digest('hex');
    }

    static getUser(username: String, callback: Function) {
        const params = {
            TableName: tableName,
            Key: {
                id: hash({username: username}),
                username: username
            }
        };
        db.getClient().get(params, (err, data) => {
            if (err) {
                callback(err, data);
            } else if (data.Item) {
                let user = User.createUser(data.Item);

                callback(err, user);
            } else {
                callback({name: 'User not found'}, null);
            }
        });
    }

    static getUsers(callback: Function) {
        let params = {
            TableName : tableName,
        };

        db.getClient().scan(params, function(err, data) {
            if (err) {
                console.error('Unable to query. Error:', JSON.stringify(err, null, 2));
                callback(err, []);
            } else {
                callback(err, User.processData(data.Items));
            }
        });
    }

    static getUsersByGroup(groupid: String, callback: Function) {
        let params = {
            TableName: tableName,
            FilterExpression: 'contains(#groups, :group)',
            ExpressionAttributeNames: {
                '#groups': 'groups',
            },
            ExpressionAttributeValues: {
                ':group': groupid,
            }
        };

        db.getClient().scan( params, function ( err, data ) {
            if ( err ) {
                console.error( 'Unable to query. Error:', JSON.stringify( err, null, 2 ) );
                callback( err, [] );
            } else {
                callback( err, User.processData(data.Items) );
            }
        } );
    }

    private static processData(items: any) {
        let users: User[] = [];
        items.forEach((data: any) => {
            users.push(User.createUser(data));
        });
        return users;
    }

    private static createUser(data: any) {
        return new User(data.username, data.name, data.mail, data.address, data.city, data.country, data.guarantors,
            data.groups, parseFloat(data.balance), data.id);
    }

    constructor(username: String, name: String, mail: String, address: String, city: String, country: String,
                guarantors: String[] = [], groups: String[] = [], balance: Number = 0, id?: String) {
        this.username = username;
        this.name = name;
        this.mail = mail;
        this.address = address;
        this.city = city;
        this.country = country;
        this.guarantors = guarantors;
        this.groups = groups;
        this.balance = parseFloat(balance.toString());
        if (id) {
            this.id = id;
        }
    }

    create(password: string, callback: Function) {
        const params = {
            TableName: tableName,
            Item: {
                'id': hash({username: this.username}),
                'username': this.username,
                'name': this.name,
                'password': User.createPasswordHash(password),
                'mail': this.mail,
                'address': this.address,
                'city': this.city,
                'country': this.country,
                'guarantors': this.guarantors,
                'groups': this.groups,
                'balance': parseFloat(this.balance.toString()),
            }
        };
        db.getClient().put(params, (err, data) => {
            if (err) {
                console.error('Unable to add item. Error JSON:', JSON.stringify(err, null, 2));
                callback(err, data);
            } else {
                console.log('Added item:', JSON.stringify(data, null, 2));
                this.id = params.Item.id;
                callback(err, this);
            }
        });
    }

    update(callback: Function) {
        const params = {
            TableName: tableName,
            Key: {
                'id': this.id,
                username: this.username
            },
            UpdateExpression: 'set #name = :name, mail= :mail, address= :address, city = :city, country = :country, ' +
            'guarantors = :guarantors, groups = :groups',
            ExpressionAttributeNames: {
                '#name': 'name'
            },
            ExpressionAttributeValues: {
                ':name': this.name,
                ':mail': this.mail,
                ':address': this.address,
                ':city': this.city,
                ':country': this.country,
                ':guarantors': this.guarantors,
                ':groups': this.groups
            },
            ReturnValues: 'UPDATED_NEW'
        };

        db.getClient().update(params, function(err, data) {
            if (err) {
                console.error('Unable to update item. Error JSON:', JSON.stringify(err, null, 2));
                callback(err, null);
            } else {
                console.log('UpdateItem succeeded:', JSON.stringify(data, null, 2));
                callback(null, data);
            }
        });
    }

    updateBalance(change: number, callback?: Function) {
        const params = {
            TableName: tableName,
            Key: {
                'id': this.id,
                username: this.username
            },
            UpdateExpression: 'set balance = balance + :change',
            ExpressionAttributeValues: {
                ':change': change,
            },
            ReturnValues: 'UPDATED_NEW'
        };

        db.getClient().update(params, (err, data) => {
            if (err) {
                console.error('Unable to update item. Error JSON:', JSON.stringify(err, null, 2));
                if (callback) callback(err, data);
            } else {
                console.log('UpdateItem succeeded:', JSON.stringify(data, null, 2));
                if (callback) callback(null, this);
            }
        });
    }

    fetchGroups() {
        let promise: Promise<String>[] = [];
        this.groups.forEach((groupId) => {
            promise.push(new Promise((resolve, reject) => {
                Group.getGroup(groupId, (err: any, data: Group) => {
                    if (err) {
                        reject(err);
                    }
                    this.groupData.push(data);
                    resolve();
                });
            }));
        });

        return Promise.all(promise).then(() => {
            return this;
        });
    }

    fetchLoans() {
        return new Promise((resolve, reject) => {
            Loan.getLoansFromUser(this.username, (err: any, loans: Loan[]) => {
                if (err) {
                    reject(err);
                }
                this.loansData = loans;
                resolve(this);
            });
        }).then(() => {
            return this;
        });
    }

    fetchInvestments() {
        return new Promise((resolve, reject) => {
            Investment.getInvestmentsFromUser(this.username, (err: any, investments: Investment[]) => {
                if (err) {
                    reject(err);
                }
                this.investmentsData = investments;
                resolve(this);
            });
        }).then(() => {
            return this;
        });
    }

    fetchRequests() {
        return new Promise((resolve, reject) => {
            Request.getRequestsFromUser(this.username, (err: any, requests: Request[]) => {
                if (err) {
                    reject(err);
                }
                this.requestsData = requests;
                resolve(this);
            });
        }).then(() => {
            return this;
        });
    }
}