import { Group } from './group'
export interface Loan {
    amount: number
    username: string
    group: string
    interest: 0.16
    length: 2
    status: "Running"
}