import { User } from "model/user";

export interface Group {
    id: string
    name: string
    country: string
    members: User[]
    isPublic: boolean
    trustService: string
    totalFunds: number
    availableFunds: number
}