import { Group } from './group'
import { Loan } from './loan'

export interface User {
    address: string
    balance: number
    city: string
    country: string
    groups: string[]
    loansData: Loan[]
    groupData: Group[]
    guarantors: any
    id: string
    mail: string
    name: string
    username: string
}