import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Group } from 'model/group';
import { Http } from '@angular/http';

@Component( {
  selector: 'group-list',
  templateUrl: './group-list.component.html',
  styleUrls: ['./group-list.component.css']
} )
export class GroupListComponent implements OnInit {

  @Output()
  onClickedRow = new EventEmitter();
  allGroups: Array<Group>

  constructor( private http: Http ) { }

  ngOnInit() {
    // Make the HTTP request to gather all the groups
    this.http.get( 'group' ).subscribe( groups => {
      this.allGroups = groups.json()
      this.onClickedRow.emit( this.allGroups[0] )
    } )
  }

}
