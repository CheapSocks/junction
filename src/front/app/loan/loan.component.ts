import { Input, Component, OnInit } from '@angular/core';
import { Loan } from '../../model/loan'

@Component( {
  selector: 'app-loan',
  templateUrl: './loan.component.html',
  styleUrls: ['./loan.component.css']
} )
export class LoanComponent implements OnInit {
  @Input()
  loan: Loan
  constructor() { }

  ngOnInit() {
  }

}
