import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


// import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component'
import { BalanceComponent } from './balance/balance.component'
import { GroupComponent } from './group/group.component'
import { GroupListComponent } from './group-list/group-list.component'
import { HistoryComponent } from './history/history.component'
import { ProfileComponent } from './profile/profile.component'
import { LoginComponent } from './login/login.component'
import { EnterComponent } from './enter/enter.component'
import { LoanComponent } from './loan/loan.component'

@NgModule( {
  declarations: [
    DashboardComponent,
    BalanceComponent,
    GroupComponent,
    GroupListComponent,
    HistoryComponent,
    ProfileComponent,
    LoginComponent,
    EnterComponent,
    LoanComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],

  providers: [],
  bootstrap: [LoginComponent]

} )
export class AppModule { }
