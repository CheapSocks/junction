import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { Http } from '@angular/http';


@Component( {
  selector: 'app-enter',
  templateUrl: './enter.component.html',
  styleUrls: ['./enter.component.css']

} )
export class EnterComponent implements OnInit {
  username: string
  password: string

  @Output()
  onSubmission = new EventEmitter()

  constructor(private http: Http ) { }
  update(username, password){
    this.onSubmission.emit([username, password])    
  }

  ngOnInit() {
  }

  click() {
    console.log( this.username )
    console.log( this.password )
  }
}
