import { Input, Component, OnInit } from '@angular/core';
import { User } from '../../model/user'
@Component( {
  selector: 'balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.css']
} )
export class BalanceComponent implements OnInit {
  @Input()
  awaibleFunds: number
  @Input()
  pendingFunds: number
  @Input()
  totalFunds: number
  @Input()
  loanFunds: number
  constructor() { }

  ngOnInit() {
  }
  sum() {
    return this.awaibleFunds + this.loanFunds + this.pendingFunds + this.totalFunds
  }
  percent( value: number ) {
    return Math.random() * 160 % 100 + '%'

  }

}
