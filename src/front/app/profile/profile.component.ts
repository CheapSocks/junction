import { Input, Component, OnInit } from '@angular/core';
import { User } from '../../model/user'

@Component( {
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
} )
export class ProfileComponent implements OnInit {
  @Input()
  user: User
  constructor() { }

  ngOnInit() {
  }

  loanMoney() {
    return this.user.loansData.reduce( ( sum, v ) => sum + v.amount, 0 ).toFixed(2)
  }
  totalMoney() {
    return this.user.groupData.reduce((sum, v) => sum + v.totalFunds, 0).toFixed(2);
  }
  awaibleMone() {
    return this.user.groupData.reduce((sum, v) => sum + v.availableFunds, 0).toFixed(2);
  }
  pendingMoney() {
    return this.user.balance.toFixed(2)
  }

}
