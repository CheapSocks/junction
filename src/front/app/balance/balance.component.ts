import { Input, Component, OnInit } from '@angular/core';
import { User } from '../../model/user'
@Component( {
  selector: 'balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.css']
} )
export class BalanceComponent implements OnInit {
  @Input()
  awaibleFunds: number // user.balance
  @Input()
  pendingFunds: number // Not in prototype: requested but not accepted
  @Input()
  totalFunds: number // sum: user.investmentData[].amount
  @Input()
  loanFunds: number // sum: user.loanData[].amount
  constructor() { }

  ngOnInit() {
  }

}
