import { Component, OnInit } from '@angular/core';
import { fakeAsync } from '@angular/core/testing';
import { User } from 'model/user';
import { Http } from '@angular/http';

@Component( {
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
} )
export class LoginComponent implements OnInit {
  isLogin = true
  user: User = {
    address: '',
    balance: 0,
    city: '',
    country: '',
    groups: [],
    loansData: [],
    groupData: [],
    guarantors: undefined,
    id: '',
    mail: '',
    name: '',
    username: '',
  }
  wrongLogin = false
  constructor( private http: Http ) { }

  ngOnInit() {
  }

  submitted( [password, username] ) {
    const tempUser = {
      "username": username,
      "password": password
    }
    console.log( "ok from login" )
    console.log( tempUser )
    // Make the HTTP request a user data   
    this.http.post( 'user/' + tempUser.username + '/login', tempUser ).subscribe( data => {
      // Read the result field from the JSON response.
      this.user = data.json()
      this.wrongLogin = false
      this.isLogin = false
    }, e => this.wrongLogin = true );
  }
}
