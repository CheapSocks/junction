import { Input, Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Group } from '../../model/group';

import { Http, HttpModule } from '@angular/http';

import { User } from '../../model/user'

@Component( {
  selector: 'group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css'],
  providers: [HttpModule]
} )
export class GroupComponent implements OnInit {
  loanAmount = 0
  loanLengh = 1
  @Input()
  group: Group
  @Input()
  user: User
  isLoanInvest = true
  isInvestActive = true
  @Output()
  refresh = new EventEmitter()

  constructor( private http: Http ) { }
  getBalance() {
    console.log( this.group.availableFunds )
    return this.group.availableFunds //this.user.loansData.reduce( ( sum, loan ) => loan.group == this.group.id ? loan.amount + sum : sum, 0 )
  }
  ngOnInit() {
  }
  isMember() {
    return this.user.groups.findIndex( id => id == this.group.id ) != -1
  }
  join() {
    this.http.put(
      `user/${this.user.username}`,
      {
        groups: this.user.groups.concat( this.group.id ),
      } ).subscribe( data => {
        this.refresh.emit()
        // Read the result field from the JSON response.
      }, err => console.log( err._body ) );

  }
  leave() {
    this.http.put(
      `user/${this.user.username}`,
      {
        groups: this.user.groups.filter( id => id != this.group.id ),
      } ).subscribe( data => {
        this.refresh.emit()
        // Read the result field from the JSON response.
      }, err => console.log( err._body ) );
  }
  makeInvestment( amount: number ) {
    // console.log( this.user.groups.findIndex( id => id == this.group.id ) )
    this.http.post(
      `group/${this.group.id}/invest`,
      {
        username: this.user.username,
        amount: amount
      } ).subscribe( data => {
        this.refresh.emit()
        // Read the result field from the JSON response.
      }, err => console.log( err._body ) );
  }
  sqrt( v ) {
    return Math.sqrt( v )
  }
  interest( loanAmount, loanLengh ) {
    return ( loanAmount > 0 && loanLengh > 0 ) ? ( 5 + Math.sqrt( loanAmount ) / 50 + Math.sqrt( loanLengh ) ).toFixed( 1 ) : 6.5
  }
  takeLoan() {
    this.http.post(
      `group/${this.group.id}/loan`,
      {
        username: this.user.username,
        amount: this.loanAmount,
        interest: this.interest( this.loanAmount, this.loanLengh ),
        length: this.loanLengh
      } ).subscribe( data => {
        this.refresh.emit()
        // Read the result field from the JSON response.
      }, err => console.log( err ) );
  }
}
