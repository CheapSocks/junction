import { Input, Component, OnInit, EventEmitter, Output, } from '@angular/core';
import { Http, HttpModule } from '@angular/http';
import { User } from '../../model/user'
import { Group } from '../../model/group';

@Component( {
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [HttpModule]
} )
export class DashboardComponent {
  @Input()
  user: User

  activeGroup: Group = {
    id: '',
    name: '',
    country: '',
    members: [],
    isPublic: true,
    trustService: '',
    totalFunds: 0,
    availableFunds: 0
  }
  constructor( private http: Http ) { }


  ngOnInit(): void {

  }

  setClickedRow( group: Group ): void {
    this.activeGroup = group;
  }
  updateUser() {
    this.http.get( 'user/' + this.user.username ).subscribe( data => {
      // Read the result field from the JSON response.
      this.user = data.json()
    } )
  }

}
