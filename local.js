const aws = require('aws-sdk');
const credentials = require('./.aws/credentials.json');
aws.config.update({
        'credentials': {
            'accessKeyId': credentials.aws_access_key_id,
            'secretAccessKey': credentials.aws_secret_access_key
        },
        'region': credentials.region
    });

const app = require('./dist/app.js');
const port = process.env.PORT || 8000;

// Server
app.listen(port, () => {
    console.log(`Listening on: http://localhost:${port}`);
});
